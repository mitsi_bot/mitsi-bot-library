import BaseService from '../src/BaseService';

jest.mock("../src/PrometheusMiddleware", () => {
    return function() {
      return { makeApiMiddleware: Function.prototype };
    };
  });

jest.mock("express");
const mockedExpress = require("express");
import { Request, Response } from 'express';

class TestServer extends BaseService {
    onPost(_request: Request, response: Response): void {
        response.json({ result: "ok" });
    }
}

describe("Base service test", () => {
    beforeEach(() => {
        mockedExpress.mockClear();
        mockedExpress.set = jest.fn();
        mockedExpress.use = jest.fn();
        mockedExpress.get = jest.fn();
        mockedExpress.post = jest.fn();
        mockedExpress.listen = jest.fn();

    })
    it("it should listen on 9001 by default", () => {
        new TestServer(mockedExpress).startServer();

        expect(mockedExpress.set).toHaveBeenCalledWith("port", 9001);
    });

    it("it should listen on get /", () => {
        new TestServer(mockedExpress).startServer();
        expect(mockedExpress.get.mock.calls[0]).toEqual(["/", expect.any(Function)]);
    });

    it("it should call onPost on post /", done => {
        const server = new TestServer(mockedExpress);
        server.startServer();
        const response = {json: (r: string) => {
            expect(r).toEqual({ result: "ok" });
            done();
        }};
        server.onPost({} as Request, response as Response);
    });
});