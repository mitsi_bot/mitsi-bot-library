const url = require("url");
import { Request, Response, Application } from "express";
const promBundle = require("express-prom-bundle");
const listEndpoints = require("express-list-endpoints");
const promClient = require("prom-client");
const UrlPattern = require("url-pattern");

interface IOption {
    host?: string,
    port?: number,
    urlPatternMaker?: Function,
    normalizePath?: string,
    discardUnmatched?: boolean,
    createServer?: boolean
}

interface IRoute {
    path: string,
    pattern: string,
    methods: string
}

export default class PrometherusMiddleware {

    private app: Application;
    private path: string;

    constructor(app: Application, path: string) {
        this.app = app;
        this.path = path;
    }

    setupMetricService = () => {
        this.app.get(this.path, (_req: Request, res: Response) => {
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end(promClient.register.metrics());
        });
    };

    private captureAllRoutes = (option: IOption, app: Application) => {
        let allRoutes = listEndpoints(app);
        allRoutes = allRoutes.filter(
            (route: IRoute) => route.path !== "/*" && route.path !== "*"
        );

        allRoutes.forEach((route: IRoute) => {
            route.path = removeTrailingSlash(route.path);

            console.log(`Route found: ${route.path}`);
            route.pattern = route.path;

            // NOTE: urlPatternMaker has to create an UrlPattern compatible object.
            if (option.urlPatternMaker) {
                route.path = option.urlPatternMaker(route.path);
            } else {
                route.path = new UrlPattern(route.path, {
                    segmentNameCharset: "\/a-zA-Z0-9_-"
                });
            }
        });

        return allRoutes;
    };

    makeApiMiddleware = (option: IOption = {}) => {
        const allRoutes: IRoute[] = [];

        const normalizePath = (req: Request, _opts: IOption) => {
            if (option.normalizePath !== undefined && !option.normalizePath) {
                return req.url;
            }

            let pattern = null;
            let path = url.parse(req.originalUrl || req.url).pathname;
            path = removeTrailingSlash(path);

            allRoutes.some(route => {
                if (route.methods.indexOf(req.method) === -1) {
                    return false;
                }

                if (route.path.match(path)) {
                    pattern = route.pattern;
                    return true;
                }

                return false;
            });

            if (option.discardUnmatched && pattern === null) {
                return false;
            }

            return pattern || path;
        };

        const metricsMiddleware = promBundle({
            includeMethod: true,
            includePath: true,
            autoregister: false,
            buckets: [0.03, 0.3, 1, 1.5, 3, 5, 10],
            normalizePath
        });

        this.setupMetricService();

        return (req: Request, res: Response, next: Function) => {
            if (allRoutes.length === 0) {
                this.captureAllRoutes(option, req.app).forEach((route: IRoute) => {
                    if(route.path !== this.path){
                        allRoutes.push(route);
                    }
                });
            }
            metricsMiddleware(req, res, next);
        };
    };
}

function removeTrailingSlash(path: string): string {
    if (path.endsWith("/") && path !== "/") {
        return path.replace(/\/$/, "");
    }
    return path;
}