export default class LanguageProcessingApiInput {
    public message: string;
    public isAuthenticated: boolean;

    constructor(message: string, isAuthenticated: boolean) {
        this.message = message;
        this.isAuthenticated = isAuthenticated;
    }
}