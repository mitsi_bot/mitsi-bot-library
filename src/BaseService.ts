import { Response, Request, Application } from "express";
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require('config-yml');
const promClient = require('prom-client');
import PrometheusMiddleware from './PrometheusMiddleware'

export default abstract class BaseService {
    private app: Application;

    constructor(app: Application) {
        this.app = app;
        this.app.use(new PrometheusMiddleware(this.app, "/metrics").makeApiMiddleware());
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
    }

    /**
     * Main method that map POST request on /
     * @param request
     * @param response 
     * Must return smt like response.json(new ResponseText("Hello World"));
     */
    abstract onPost(request: Request, response: Response): void;

    /**
     * startServer
     * /!\ The port is configured in config.yml app.port.
     * 9001 by default
     */
    startServer() {
        this.app.get("/", (_: Request, response: Response) => {
            response.json({ status: "UP" });
        });

        this.app.post("/", (request: Request, response: Response) => {
            this.onPost(request, response);
        });

        const port = config && config.app && config.app.port || 9001;
        this.app.set("port", port);
        this.app.listen(this.app.get("port"), () => {
            console.log(`Express running → PORT ${port}`);
            if (config.webservices) {
                console.log("Available services are", config.webservices);
            }
        });
    }
}

