export function formatNumber(number: number): string {
  return ("0" + number).slice(-2);
}

export function convertSecondsToDHMS(totalSeconds: number): { days: number, hours: number, minutes: number, seconds: number } {
  const days = Math.trunc(totalSeconds / (24 * 3600));
  const hours = Math.trunc((totalSeconds - days * 24 * 3600) / 3600);
  const minutes = Math.trunc((totalSeconds - (days * 24 * 3600 + hours * 3600)) / 60);
  const seconds = Math.trunc(totalSeconds - (days * 24 * 3600 + hours * 3600 + minutes * 60));
  return { days, hours, minutes, seconds };
}

export function destructDate(modifiedDate: Date): { months: string, days: string, years: string, hours: string, minutes: string, seconds: string } {
  const months = formatNumber(modifiedDate.getUTCMonth() + 1);
  const days = formatNumber(modifiedDate.getUTCDate());
  const years = modifiedDate.getUTCFullYear().toString();
  const hours = formatNumber(modifiedDate.getHours());
  const minutes = formatNumber(modifiedDate.getMinutes());
  const seconds = formatNumber(modifiedDate.getSeconds());
  return { months, days, years, hours, minutes, seconds };
}