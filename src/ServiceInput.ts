export default class ServiceInput {
    public parameters?: any;

    constructor(parameters?: any) {
        this.parameters = parameters;
    }
}
