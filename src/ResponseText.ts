export default class ResponseText {
    public displayText: string;
    public speech: string;
  
    constructor(text: string) {
      this.displayText = text;
      this.speech = text;
    }
  }