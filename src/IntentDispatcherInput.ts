export default class IntentDispatcherInput {
    public intent: string;
    public parameters?: any;
    public isAuthenticated: boolean;

    constructor(intent: string,  isAuthenticated: boolean, parameters?: any) {
        this.intent = intent;
        this.parameters = parameters;
        this.isAuthenticated = isAuthenticated;
    }
}
